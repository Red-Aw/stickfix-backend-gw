FROM php:7.4-fpm-alpine3.13

RUN docker-php-ext-install pdo_mysql

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

WORKDIR /app

COPY . /app

RUN composer install --ignore-platform-reqs

CMD php artisan serve --host=0.0.0.0 --port=8081

EXPOSE 8081