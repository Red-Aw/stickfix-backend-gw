<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insertGetId([
            'tempId' => 1,
            'username' => 'admin',
            'email' => 'admin@stickfix.store',
            'password' => Hash::make('randompassword'),
            'confirmed' => true,
        ]);
    }
}
