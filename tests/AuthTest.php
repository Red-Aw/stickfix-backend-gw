<?php

use App\Models\User;

class AuthTest extends TestCase
{
    // Uncomment unique key generation in AuthController
    // To test please run commands:
    // php artisan migrate:fresh --seed
    // php artisan passport:install --force
    // Restart server and run command:
    // vendor/bin/phpunit

    public function testLoginAndLogout()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'tokenType' => 'bearer',
                'expiresIn' => 86400,
            ]);
    }

    public function testLogout()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/logout', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200);
    }

    public function testUser()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/user', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testCheckAuth()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';

        $this->json('GET', 'api/checkAuth', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => false,
            ]);

        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/checkAuth', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testCheckAuthWithRole()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';

        $this->json('GET', 'api/checkAuth/superAdmin' , [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => false,
            ]);

        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/checkAuth/superAdmin' , [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->json('GET', 'api/checkAuth/addNewUser', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function clearTableAtEnd()
    {
        User::where('id', '<>', 1)->delete();
    }
}
