<?php

class EnumTest extends TestCase
{
    public function testGetCategories()
    {
        $this->json('GET', 'api/getCategories', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetStates()
    {
        $this->json('GET', 'api/getStates', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetMessageOptions()
    {
        $this->json('GET', 'api/getMessageOptions', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetServiceTypes()
    {
        $this->json('GET', 'api/getServiceTypes', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetBrands()
    {
        $this->json('GET', 'api/getBrands', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetLanguages()
    {
        $this->json('GET', 'api/getLanguages', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetAllEnums()
    {
        $this->json('GET', 'api/getAllEnums', [], [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }
}
