<?php

use App\Models\User;

class UserTest extends TestCase
{
    public function testStoreUser()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'DojesJon',
            'email' => 'doje123@example.com',
            'password' => 'test12345',
            'password_confirmation' => 'test12345',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' ])
            ->seeStatusCode(401);

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testAlreadyTakenEmail()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'RandomUser',
            'email' => 'randomuser@example.com',
            'password' => 'randompassword',
            'password_confirmation' => 'randompassword',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $data = [
            'email' => 'randomuser@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testAvailableEmail()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'RandomUserz',
            'email' => 'randomuserz@example.com',
            'password' => 'randompassword',
            'password_confirmation' => 'randompassword',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $data = [
            'email' => 'availableemail@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => true,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testChangeUserPassword()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'quentin.rogahn26',
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => 'quentin.rogahn26123',
            'password_confirmation' => 'quentin.rogahn26123',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $data = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $user = User::where('email', '=', $userData['email'])->get()[0];
        $newPasswordData = [
            'id' => $user->tempId,
            'currentPassword' => 'quentin.rogahn26123',
            'newPassword' => 'gwendolyn123',
            'newPassword_confirmation' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/newPass', $newPasswordData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200);

        $this->clearTableAtEnd();
    }

    public function testChangeUserLanguage() {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'quentinogahn',
            'email' => 'quentin.rogahn@hotmail.com',
            'password' => 'quentin.rogahn123',
            'password_confirmation' => 'quentin.rogahn123',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $data = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $user = User::where('email', '=', $userData['email'])->get()[0];
        $newLanguageData = [
            'id' => $user->id,
            'language' => 'en',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/changeLanguage', $newLanguageData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200);

        $this->clearTableAtEnd();
    }

    public function testCheckPasswordsMatch()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'qoberbrunner41',
            'email' => 'zora.oberbrunner41@yahoo.com',
            'password' => 'oberbrunner41123',
            'password_confirmation' => 'oberbrunner41123',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $data = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $user = User::where('email', '=', $userData['email'])->get()[0];
        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' ])
            ->seeStatusCode(200);

        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41yxz',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'match' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testCreateUser()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $userData = [
            'username' => 'gpowerhd',
            'email' => 'gpowerhd@gmail.com',
            'password' => 'test12345',
            'password_confirmation' => 'test12345',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' ])
            ->seeStatusCode(401);

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function clearTableAtEnd()
    {
        User::where('id', '<>', 1)->delete();
    }
}
