<?php

class ProductTest extends TestCase
{
    public function testStoreItem()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $itemData = [
            'title' => 'Conveying or northward',
            'description' => 'Conveying or northward offending admitting perfectly my. Colonel gravity get thought fat smiling add but. Wonder twenty hunted and put income set desire expect. Am cottage calling my is mistake cousins talking up. Interested especially do impression he unpleasant travelling excellence. All few our knew time done draw ask.',
            'price' => 24.68,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'NO',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '1.5 - EUR 34',
            'skateWidth' => 'D',
            'images' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/createItem', $itemData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testEditItem()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $editedItemData = [
            'id' => 1,
            'title' => 'Far curiosity',
            'description' => 'Far curiosity incommode now led smallness allowance. Favour bed assure son things yet. She consisted consulted elsewhere happiness disposing household any old the. Widow downs you new shade drift hopes small. So otherwise commanded sweetness we improving. Instantly by daughters resembled unwilling principle so middleton. Fail most room even gone her end like. Comparison dissimilar unpleasant six compliment two unpleasing any add. Ashamed my company thought wishing colonel it prevent he in. Pretended residence are something far engrossed old off.',
            'price' => 60.50,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '3.0 - EUR 36',
            'skateWidth' => 'R',
            'newImages' => [],
            'storedImages' => [],
            'deletedImages' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/editItem', $editedItemData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testMarkItem()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/markItem/1', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testDeleteItem()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/deleteItem/1', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetItems()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $firstItemData = [
            'title' => 'entreaties Dispatched',
            'description' => 'entreaties boisterous. Certain forbade picture now prevent carried she get see sitting. Up twenty limits as months. Inhabit so perhaps of in to certain. Sex excuse chatty was seemed warmth. Nay add far few immediate sweetness earnestly dejection.',
            'priceInMinorUnit' => 2999,
            'category' => 'HELMET',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'CCM',
            'size' => 'XS',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'images' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/createItem', $firstItemData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $secondItemData = [
            'title' => 'unsatiable Old',
            'description' => 'Old impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all. Very mrs shed shew gave you. Oh shutters do removing reserved wandered an. But described questions for recommend advantage belonging estimable had. Pianoforte reasonable as so am inhabiting. Chatty design remark and his abroad figure but its.',
            'priceInMinorUnit' => 12999,
            'category' => 'BAGS',
            'isActive' => 0,
            'state' => 'USED',
            'brand' => 'CCM',
            'size' => 'S',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'images' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/createItem', $secondItemData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->json('GET', 'api/getItems', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetItem()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $itemData = [
            'title' => 'Cause dried no solid',
            'description' => 'Cause dried no solid no an small so still widen. Ten weather evident smiling bed against she examine its. Rendered far opinions two yet moderate sex striking. Sufficient motionless compliment by stimulated assistance at. Convinced resolving extensive agreeable in it on as remainder. Cordially say affection met who propriety him. Are man she towards private weather pleased. In more part he lose need so want rank no. At bringing or he sensible pleasure. Prevent he parlors do waiting be females an message society.',
            'priceInMinorUnit' => 7500,
            'category' => 'STICKS',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => 120,
            'bladeCurve' => 'P88',
            'bladeSide' => 'NONE',
            'stickSize' => 'JR',
            'skateLength' => '',
            'skateWidth' => '',
            'images' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/createItem', $itemData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->json('GET', 'api/getItem/2', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

}
