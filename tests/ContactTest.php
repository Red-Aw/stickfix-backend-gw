<?php

class ContactTest extends TestCase
{
    public function testSendMessage()
    {
        $messageData = [
            'name' => 'reziac',
            'email' => 'reziac@yahoo.com',
            'phone' => [
                'number' => '123456789',
                'country' => 'EU',
                'isValid' => 1,
            ],
            'description' => 'In it except to so temper mutual tastes mother. Interested cultivated its continuing now yet are. Out interested acceptance our partiality affronting unpleasant why add. Esteem garden men yet shy course. Consulted up my tolerably sometimes perpetual oh. Expression acceptance imprudence particular had eat unsatiable.',
            'radioOption' => 'APPLY_FOR_REPAIR',
            'language' => 'ru',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/sendMessage', $messageData, [ 'Accept' => 'application/json' ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testCreateReply()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $replyData = [
            'id' => 1,
            'newReplyText' => 'Nor hence hoped her after other known defer his. For county now sister engage had season better had waited.',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/replyOnMessage', $replyData, [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetMessages()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/getMessages', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }

    public function testGetMessage()
    {
        $data = [
            'email' => 'admin@stickfix.store',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('APP_URL') . '/api/login', [ 'form_params' => $data ]);

        $authToken = '';
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (is_array($content) && array_key_exists('success', $content) && array_key_exists('accessToken', $content)) {
                $authToken = $content['accessToken'];
            }
        }

        $this->json('GET', 'api/getMessage/1', [], [ 'Accept' => 'application/json', 'Authorization' => 'Bearer ' . $authToken ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);
    }
}
