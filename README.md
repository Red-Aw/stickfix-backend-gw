StickFix Gateway. Built with Lumen 8. Built by Nethall

---------------------------------------------------------------

To deploy:
1. add .env file. APP_ENV=production, APP_DEBUG=false, add correct URLs, DB connection, reCaptcha keys, IG key, JTW secret, and microservice URLs and secrets.
2. Follow this: https://laravel.com/docs/8.x/deployment


Testing:
1. make sure .env.testing is set with testing db and configurations
2. switch to testing environment with command: php artisan config:cache --env=testing
3. seed db with default values: php artisan migrate:fresh --seed
4. run tests: php artisan test

---------------------------------------------------------------

Commands:


Generate key:
* php artisan key:generate

<br />

Generate LumenPassport secrets:
* php artisan passport:install

<br />

Make controller:
* php artisan make:controller NameController
* php artisan make:controller NameController --resource // adds crud methods

<br />

Refresh DB and seed DB:
* php artisan migrate:fresh --seed

<br />

Manual refresh DB and seed DB:
* php artisan migrate
* php artisan db:seed

<br />

Create Table:
* php artisan make:migration create_new_table

<br />

Create Model:
* php artisan make:model Name -m

<br />

Shows All Routes:
* php artisan route:list

<br />

Create Middleware:
* php artisan make:middleware Name
* after creating please register in: app/Http/Kernel.php

<br />

Create New Factory:
* php artisan make:factory FactoryName

<br />

Create New Test:
* php artisan make:test UserTest

<br />

Run Tests:
* vendor/bin/phpunit

Change variables in phpunit.xml. Set correct test DB

<br />

Laravel Version
* php artisan --version

<br />

Commands for local development (without docker):
* php artisan serve --port=8081

<br />

Clear cache
* php artisan cache:clear