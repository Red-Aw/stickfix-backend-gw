<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class InstagramApi extends Controller
{
    const URL = 'https://graph.instagram.com/me/media';

    public function getInstagramFeed()
    {
        $success = true;
        $message = null;
        $data = [];

        if (env('APP_ENV') !== 'production') {
            return response()->json([
                'success' => false,
                'message' => 'IG api down',
                'posts' => [],
            ]);
        }

        try {
            if (Cache::has('getInstagramData')) {
                $data = Cache::get('getInstagramData');
            } else {
                $params = [
                    'fields' => 'caption,media_type,media_url,permalink',
                    'access_token' => config('services.instagram.accessToken'),
                ];

                $client = new \GuzzleHttp\Client();
                $response = $client->request('GET', self::URL, [ 'query' => $params ]);

                if ($response->getStatusCode() === 200) {
                    $success = true;
                    $content = json_decode($response->getBody(), true);

                    if (is_array($content) && array_key_exists('data', $content)) {
                        $data = array_slice($content['data'], 0, 5);
                        Cache::put('getInstagramData', $data, Carbon::now()->addMinutes(10));
                    }
                } else {
                    $success = false;
                    $message = 'error.errorReceivingInstagramFeed';
                }
            }
        } catch (\Exception $e) {
            $success = false;
            $message = 'Error receiving Instagram feed [' . $e->getMessage() . ']';
            $data = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'posts' => $data,
        ]);
    }
}
