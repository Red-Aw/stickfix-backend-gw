<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReCaptcha3 extends Controller
{
    const URL = 'https://www.google.com/recaptcha/api/siteverify';

    public static function verify(string $token)
    {
        if (env('APP_ENV') === 'testing') {
            return true;
        }

        $params = [
            'secret' => config('services.recaptcha.secret'),
            'response' => $token,
        ];

        $options = [
            'http' => [
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'method'  => 'POST',
                'content' => http_build_query($params)
            ]
        ];

        $context  = stream_context_create($options);
        $result = file_get_contents(static::URL, false, $context);
        $resultJson = json_decode($result);

        return $resultJson->success ? true : false;
    }
}
