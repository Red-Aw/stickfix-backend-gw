<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;

use App\Models\User;

use App\Http\Controllers\ReCaptcha3;
use App\Http\Controllers\UserController;

use Carbon\Carbon;

class AuthController extends Controller
{
    private $userController;

    public function __construct(UserController $userController)
    {
        $this->userController = $userController;
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required',
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.incorrectCredentials', [], Response::HTTP_OK, $validator->errors());
        }

        $loginType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $user = User::where($loginType, $request->email)->first();

        // Use when testing
        // $nowInMilliseconds = (int) (Carbon::now()->timestamp . str_pad(Carbon::now()->milli, 3, '0', STR_PAD_LEFT));
        // $token = $user->createToken($nowInMilliseconds);

        $token = $user->createToken('Auth Token');
        if ($token) {
            return $this->respondWithToken($token);
        }

        return $this->response(false, 'error.incorrectCredentials', [], Response::HTTP_OK, null);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'success' => true,
            'accessToken' => $token->accessToken,
            'tokenType' => 'bearer',
            'expiresIn' => 86400
        ]);
    }

    public function me()
    {
        return $this->userController->getUser(auth()->user()->tempId);
    }

    public function logout()
    {
        try {
            auth()->user()->tokens()->each(function ($token) {
                $token->delete();
            });
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        return $this->response(true);
    }

    public function checkAuth($role = null)
    {
        if (auth()->check()) {
            if ($role !== null) {
                return $this->userController->checkUserRole(auth()->user()->tempId, $role);
            } else {
                return $this->response(true);
            }
        }

        return $this->response(false);
    }
}
