<?php

namespace App\Http\Controllers;

use App\Services\ContactService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    private $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function getMessages()
    {
        return $this->contactService->getMessages();
    }

    public function getMessage($id)
    {
        return $this->contactService->getMessage($id);
    }

    public function createRequest(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->contactService->createRequest($request->all());
    }

    public function createReply(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->contactService->createReply($request->all());
    }
}
