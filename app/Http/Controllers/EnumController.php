<?php

namespace App\Http\Controllers;

use App\Services\EnumService;

class EnumController extends Controller
{
    private $enumService;

    public function __construct(EnumService $enumService)
    {
        $this->enumService = $enumService;
    }

    public function getCategories()
    {
        return $this->enumService->getCategories();
    }

    public function getStates()
    {
        return $this->enumService->getStates();
    }

    public function getMessageOptions()
    {
        return $this->enumService->getMessageOptions();
    }

    public function getServiceTypes()
    {
        return $this->enumService->getServiceTypes();
    }

    public function getBrands()
    {
        return $this->enumService->getBrands();
    }

    public function getLanguages()
    {
        return $this->enumService->getLanguages();
    }

    public function getAllEnums()
    {
        return $this->enumService->getAllEnums();
    }

    public function getEnum($enumType)
    {
        $enumResponse = json_decode($this->getAllEnums(), true);
        $enums = [];
        if (is_array($enumResponse) && array_key_exists('data', $enumResponse)) {
            $enumResponse = $enumResponse['data'];
            if ($enumType === 'SKATES.LENGTH') {
                if (is_array($enumResponse) && array_key_exists('SIZES', $enumResponse)) {
                    if (is_array($enumResponse['SIZES']) && array_key_exists('SKATES', $enumResponse['SIZES'])) {
                        if (is_array($enumResponse['SIZES']['SKATES']) && array_key_exists('LENGTH', $enumResponse['SIZES']['SKATES'])) {
                            $enums = $enumResponse['SIZES']['SKATES']['LENGTH'];
                        }
                    }
                }
            } else {
                if (is_array($enumResponse) && array_key_exists($enumType, $enumResponse)) {
                    $enums = $enumResponse[$enumType];
                }
            }
        }

        return $enums;
    }
}
