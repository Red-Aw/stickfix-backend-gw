<?php

namespace App\Http\Controllers;

use App\Services\UserService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\User;

use App\Http\Controllers\ReCaptcha3;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function store(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $response = json_decode($this->userService->createUser($request->all()), true);

        if ($response['success'] === true) {
            $tempId = $response['data']['userId'];
        } else {
            return $this->response(false, $response['message'], $response['data'], $response['code'], $response['errors']);
        }

        $user = new User;
        $user->tempId = $tempId;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->confirmed = $request->isConfirmed;
        $created = $user->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'recordAdded', [], Response::HTTP_OK, $response['errors']);
    }

    public function checkEmailAvailability(Request $request)
    {
        return $this->userService->checkEmailAvailability($request->all());
    }

    public function checkUsernameAvailability(Request $request)
    {
        return $this->userService->checkUsernameAvailability($request->all());
    }

    public function changePass(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $user = auth()->user();
        if (intval($user->tempId) !== intval($request->id)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, null);
        }

        $response = json_decode($this->userService->updatePassword($request->all()), true);

        if ($response['success'] === true) {
            $tempId = $response['data']['userId'];
        } else {
            return $this->response(false, $response['message'], $response['data'], $response['code'], $response['errors']);
        }

        $user = User::find($tempId);
        $user->password = Hash::make($request->newPassword);
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, $response['errors']);
    }

    public function changeLanguage(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->userService->updateLanguage($request->all());
    }

    public function checkPasswordMatch(Request $request)
    {
        $user = auth()->user();
        if ($user->tempId !== $request->id) {
            return $this->response(true, null, ['match' => false], Response::HTTP_OK, []);
        }

        return $this->userService->checkPasswordMatch($request->all());
    }

    public function getUsers()
    {
        if (auth()->check()) {
            return $this->userService->getUsers();
        }

        return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
    }

    public function getUser($id)
    {
        return $this->userService->getUser($id);
    }

    public function getRoles()
    {
        if (auth()->check()) {
            return $this->userService->getRoles(auth()->user()->tempId);
        }

        return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
    }

    public function getAllRoles()
    {
        return $this->userService->getAllRoles();
    }

    public function mark($id)
    {
        $response = json_decode($this->userService->markUser($id), true);

        if ($response['success'] === true) {
            $tempId = $response['data']['userId'];
        } else {
            return $this->response(false, $response['message'], [], $response['code'], $response['errors']);
        }

        $user = User::find($tempId);
        $user->confirmed = !$user->confirmed;
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, $response['errors']);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, $response['errors']);
    }

    public function edit(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $response = json_decode($this->userService->editUser($request->all()), true);

        if ($response['success'] === true) {
            $tempId = $response['data']['userId'];
        } else {
            return $this->response(false, $response['message'], [], $response['code'], $response['errors']);
        }

        $user = User::find($tempId);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->confirmed = $request->isConfirmed;
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, $response['errors']);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, $response['errors']);
    }

    public function checkUserRole($id, $role)
    {
        return $this->userService->checkUserRole($id, $role);
    }
}
