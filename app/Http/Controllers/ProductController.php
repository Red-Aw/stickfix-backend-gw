<?php

namespace App\Http\Controllers;

use App\Services\ProductService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function getItems()
    {
        $isAuth = auth()->check() ? 1 : 0;
        return $this->productService->getItems($isAuth);
    }

    public function getItem($id)
    {
        return $this->productService->getItem($id);
    }

    public function storeItem(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->createItem($request->all());
    }

    public function markItem($id)
    {
        return $this->productService->markItem($id);
    }

    public function deleteItem($id)
    {
        return $this->productService->deleteItem($id);
    }

    public function editItem(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->editItem($request->all());
    }

    public function getServices()
    {
        $isAuth = auth()->check() ? 1 : 0;
        return $this->productService->getServices($isAuth);
    }

    public function getService($id)
    {
        return $this->productService->getService($id);
    }

    public function storeService(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->createService($request->all());
    }

    public function markService($id)
    {
        return $this->productService->markService($id);
    }

    public function deleteService($id)
    {
        return $this->productService->deleteService($id);
    }

    public function editService(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->editService($request->all());
    }

    public function getOffers()
    {
        $isAuth = auth()->check() ? 1 : 0;
        return $this->productService->getOffers($isAuth);
    }

    public function getOffer($id)
    {
        return $this->productService->getOffer($id);
    }

    public function storeOffer(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->createOffer($request->all());
    }

    public function markOffer($id)
    {
        return $this->productService->markOffer($id);
    }

    public function deleteOffer($id)
    {
        return $this->productService->deleteOffer($id);
    }

    public function editOffer(Request $request)
    {
        $rules = [
            'recaptcha' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        return $this->productService->editOffer($request->all());
    }
}
