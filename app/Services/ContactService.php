<?php

namespace App\Services;

use App\Traits\RequestService;
use function config;

class ContactService
{
    use RequestService;

    protected $baseUri;
    protected $secret;

    public function __construct()
    {
        $this->baseUri = config('services.contacts.baseUri');
        $this->secret = config('services.contacts.secret');
    }

	public function getMessages() : string
    {
        return $this->request('GET', '/api/getMessages');
    }

	public function getMessage($id) : string
    {
        return $this->request('GET', "/api/getMessage/{$id}");
    }

	public function createRequest($data) : string
    {
        return $this->request('POST', '/api/createRequest', $data);
    }

	public function createReply($data) : string
    {
        return $this->request('POST', '/api/createReply', $data);
    }
}
