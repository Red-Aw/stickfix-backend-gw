<?php

namespace App\Services;

use App\Traits\RequestService;
use function config;

class ProductService
{
    use RequestService;

    protected $baseUri;
    protected $secret;

    public function __construct()
    {
        $this->baseUri = config('services.products.baseUri');
        $this->secret = config('services.products.secret');
    }

	public function getItems($isAuth) : string
    {
        return $this->request('GET', "/api/getItems/{$isAuth}");
    }

	public function getItem($id) : string
    {
        return $this->request('GET', "/api/getItem/{$id}");
    }

	public function createItem($data) : string
    {
        return $this->request('POST', '/api/createItem', $data);
    }

	public function markItem($id) : string
    {
        return $this->request('GET', "/api/markItem/{$id}");
    }

	public function deleteItem($id) : string
    {
        return $this->request('GET', "/api/deleteItem/{$id}");
    }

	public function editItem($data) : string
    {
        return $this->request('POST', '/api/editItem', $data);
    }

	public function getServices($isAuth) : string
    {
        return $this->request('GET', "/api/getServices/{$isAuth}");
    }

	public function getService($id) : string
    {
        return $this->request('GET', "/api/getService/{$id}");
    }

	public function createService($data) : string
    {
        return $this->request('POST', '/api/createService', $data);
    }

	public function markService($id) : string
    {
        return $this->request('GET', "/api/markService/{$id}");
    }

	public function deleteService($id) : string
    {
        return $this->request('GET', "/api/deleteService/{$id}");
    }

	public function editService($data) : string
    {
        return $this->request('POST', '/api/editService', $data);
    }

	public function getOffers($isAuth) : string
    {
        return $this->request('GET', "/api/getOffers/{$isAuth}");
    }

	public function getOffer($id) : string
    {
        return $this->request('GET', "/api/getOffer/{$id}");
    }

	public function createOffer($data) : string
    {
        return $this->request('POST', '/api/createOffer', $data);
    }

	public function markOffer($id) : string
    {
        return $this->request('GET', "/api/markOffer/{$id}");
    }

	public function deleteOffer($id) : string
    {
        return $this->request('GET', "/api/deleteOffer/{$id}");
    }

	public function editOffer($data) : string
    {
        return $this->request('POST', '/api/editOffer', $data);
    }
}
