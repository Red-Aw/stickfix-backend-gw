<?php

namespace App\Services;

use App\Traits\RequestService;
use function config;

class UserService
{
    use RequestService;

    protected $baseUri;
    protected $secret;

    public function __construct()
    {
        $this->baseUri = config('services.users.baseUri');
        $this->secret = config('services.users.secret');
    }

	public function createUser($data) : string
    {
        return $this->request('POST', '/api/storeUser', $data);
    }

    public function checkEmailAvailability($data) : string
    {
        return $this->request('POST', '/api/checkEmailAvailability', $data);
    }

	public function checkUsernameAvailability($data) : string
    {
        return $this->request('POST', '/api/checkUsernameAvailability', $data);
    }

    public function updatePassword($data) : string
    {
        return $this->request('POST', '/api/newPass', $data);
    }

	public function updateLanguage($data) : string
    {
        return $this->request('POST', '/api/changeLanguage', $data);
    }

	public function checkPasswordMatch($data) : string
    {
        return $this->request('POST', '/api/checkPasswordMatch', $data);
    }

	public function getUsers() : string
    {
        return $this->request('GET', '/api/getUsers');
    }

	public function getUser($id) : string
    {
        return $this->request('GET', "/api/getUser/{$id}");
    }

    public function getRoles($id) : string
    {
        return $this->request('GET', "/api/getRoles/{$id}");
    }

	public function getAllRoles() : string
    {
        return $this->request('GET', '/api/getAllRoles');
    }

	public function markUser($id) : string
    {
        return $this->request('GET', "/api/markUser/{$id}");
    }

	public function editUser($data) : string
    {
        return $this->request('POST', "/api/editUser", $data);
    }

	public function checkUserRole($id, $role) : string
    {
        return $this->request('GET', "/api/checkUserRole/{$id}/{$role}");
    }
}
