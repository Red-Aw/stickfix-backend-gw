<?php

namespace App\Services;

use App\Traits\RequestService;
use function config;

class EnumService
{
    use RequestService;

    protected $baseUri;
    protected $secret;

    public function __construct()
    {
        $this->baseUri = config('services.enums.baseUri');
        $this->secret = config('services.enums.secret');
    }

	public function getCategories() : string
    {
        return $this->request('GET', '/api/getCategories');
    }

	public function getStates() : string
    {
        return $this->request('GET', '/api/getStates');
    }

	public function getMessageOptions() : string
    {
        return $this->request('GET', '/api/getMessageOptions');
    }

	public function getServiceTypes() : string
    {
        return $this->request('GET', '/api/getServiceTypes');
    }

	public function getBrands() : string
    {
        return $this->request('GET', '/api/getBrands');
    }

	public function getLanguages() : string
    {
        return $this->request('GET', '/api/getLanguages');
    }

	public function getAllEnums() : string
    {
        return $this->request('GET', '/api/getAllEnums');
    }
}
