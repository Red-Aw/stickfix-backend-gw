<?php

return [
    'passport' => [
        'endpoint' => env('APP_URL'),
        'clientId' => env('PASSPORT_CLIENT_ID'),
        'clientSecret' => env('PASSPORT_CLIENT_SECRET'),
    ],

    'recaptcha' => [
        'key' => env('RECAPTCHA_SITE_KEY'),
        'secret' => env('RECAPTCHA_SECRET_KEY'),
    ],

    'instagram' => [
        'accessToken' => env('INSTAGRAM_ACCESS_TOKEN'),
    ],

    'users' => [
        'baseUri' => env('USERS_SERVICE_BASE_URI'),
        'secret' => env('USERS_SERVICE_SECRET'),
    ],

    'enums' => [
        'baseUri' => env('ENUMS_SERVICE_BASE_URI'),
        'secret' => env('ENUMS_SERVICE_SECRET'),
    ],

    'products' => [
        'baseUri' => env('PRODUCTS_SERVICE_BASE_URI'),
        'secret' => env('PRODUCTS_SERVICE_SECRET'),
    ],

    'contacts' => [
        'baseUri' => env('CONTACTS_SERVICE_BASE_URI'),
        'secret' => env('CONTACTS_SERVICE_SECRET'),
    ],
];
