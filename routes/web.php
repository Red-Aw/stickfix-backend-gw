<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => 'api'], function ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/user', 'AuthController@me');
        $router->get('/logout', 'AuthController@logout');

        $router->post('/storeUser', 'UserController@store');
        $router->post('/checkEmailAvailability', 'UserController@checkEmailAvailability');
        $router->post('/checkUsernameAvailability', 'UserController@checkUsernameAvailability');
        $router->post('/newPass', 'UserController@changePass');
        $router->post('/changeLanguage', 'UserController@changeLanguage');
        $router->post('/checkPasswordMatch', 'UserController@checkPasswordMatch');
        $router->get('/getRoles', 'UserController@getRoles');
        $router->get('/getAllRoles', 'UserController@getAllRoles');
        $router->post('/editUser', 'UserController@edit');
        $router->get('/markUser/{id}', 'UserController@mark');
        $router->get('/getUser/{id}', 'UserController@getUser');
        $router->get('/getUsers', 'UserController@getUsers');

        $router->post('/createItem', 'ProductController@storeItem');
        $router->get('/markItem/{id}', 'ProductController@markItem');
        $router->get('/deleteItem/{id}', 'ProductController@deleteItem');
        $router->post('/editItem', 'ProductController@editItem');
        $router->post('/createService', 'ProductController@storeService');
        $router->get('/markService/{id}', 'ProductController@markService');
        $router->get('/deleteService/{id}', 'ProductController@deleteService');
        $router->post('/editService', 'ProductController@editService');
        $router->post('/createOffer', 'ProductController@storeOffer');
        $router->get('/markOffer/{id}', 'ProductController@markOffer');
        $router->get('/deleteOffer/{id}', 'ProductController@deleteOffer');
        $router->post('/editOffer', 'ProductController@editOffer');

        $router->get('/getMessages', 'ContactController@getMessages');
        $router->get('/getMessage/{id}', 'ContactController@getMessage');
        $router->post('/replyOnMessage', 'ContactController@createReply');
    });

    $router->post('/login', 'AuthController@login');
    $router->get('/checkAuth', 'AuthController@checkAuth');
    $router->get('/checkAuth/{role}', 'AuthController@checkAuth');

    $router->get('/getItems', 'ProductController@getItems');
    $router->get('/getItem/{id}', 'ProductController@getItem');
    $router->get('/getServices', 'ProductController@getServices');
    $router->get('/getService/{id}', 'ProductController@getService');
    $router->get('/getOffers', 'ProductController@getOffers');
    $router->get('/getOffer/{id}', 'ProductController@getOffer');

    $router->post('/sendMessage', 'ContactController@createRequest');

    $router->get('/getCategories', 'EnumController@getCategories');
    $router->get('/getStates', 'EnumController@getStates');
    $router->get('/getMessageOptions', 'EnumController@getMessageOptions');
    $router->get('/getServiceTypes', 'EnumController@getServiceTypes');
    $router->get('/getBrands', 'EnumController@getBrands');
    $router->get('/getLanguages', 'EnumController@getLanguages');
    $router->get('/getAllEnums', 'EnumController@getAllEnums');

    $router->get('/getInstagramFeed', 'InstagramApi@getInstagramFeed');

    $router->get('/', function () {
        return response()->json([ 'success' => true ], 200);
    });
});
